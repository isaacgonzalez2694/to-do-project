import React from 'react';

import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import { queryReducer, queryUserReducer } from './app/reducers/reducer';
import thunkMiddleware from 'redux-thunk';

import { TodosContainer } from './app/components/Todos';

import './App.css';
import LoginForm from './app/components/LoginForm';

const composeEnhancers = typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
  : compose;

const enhancer = composeEnhancers(applyMiddleware(thunkMiddleware));
const store = createStore(queryReducer, enhancer);
const storeUser = createStore(queryUserReducer, enhancer);

function App() {

  return (
    <Provider store={storeUser}>
      <LoginForm renderComponent={
        <Provider store={store}>
          <div className="App">
            <TodosContainer />
          </div>
        </Provider>
      }>
      </LoginForm>
    </Provider>
  );
}

export default App;
