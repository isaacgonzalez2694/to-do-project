import React, { useState } from 'react';
import { connect } from "react-redux";
import { ValidateUserRequest } from '../actions/query';

const LoginForm = (props) => {

    // React States
    const { isSubmitted } = props.store;
    const [errorMessages, setErrorMessages] = useState({});

    const { renderComponent } = props;
    
    const errors = {
        pass: "invalid username or password"
    };

    const handleSubmit = (event) => {
        //Prevent page reload
        event.preventDefault();

        var { uname, pass } = document.forms[0];

        props.dispatch(ValidateUserRequest(uname.value, pass.value));

        if (!isSubmitted){
            setErrorMessages({ name: "pass", message: errors.pass });
        }
    };

    // Generate JSX code for error message
    const renderErrorMessage = (name) =>
    name === errorMessages.name && (
        <div className="error">{errorMessages.message}</div>
    );



    const renderForm = (
        <div className="form">
            <form onSubmit={handleSubmit}>
                <div className="input-container">
                    <label>Username </label>
                    <input type="text" name="uname" required />
                    {renderErrorMessage("uname")}
                </div>
                <div className="input-container">
                    <label>Password </label>
                    <input type="password" name="pass" required />
                    {renderErrorMessage("pass")}
                </div>
                <div className="button-container">
                    <input type="submit" />
                </div>
            </form>
        </div>
    );

    return (
        <div className="app">
            <div className="login-form">
                <div className="title">Sign In</div>
                {isSubmitted ? renderComponent : renderForm}
            </div>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        store: state
    };
};

export default connect(mapStateToProps)(LoginForm);