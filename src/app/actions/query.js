import {GraphQLClient, gql } from 'graphql-request'

const startingRequest = () => {
  return {
    type: 'STARTING_REQUEST',
  };
};

const finishedRequest = response => {
  return {
    type: 'FINISHED_REQUEST',
    response: response,
  };
};

const client = new GraphQLClient('//localhost:4000/', { headers: { 'Content-Type': 'application/json' } })

const request = (query, variables) => client.request(query, variables).then((res) => res);

export const ValidateUserRequest = (user, password) => validateUser(validateUserQuery, {  
    "name": user,
    "password": password
});

const validateUser = (query, variables) => {
    return dispatch => {
      request(query, variables).then(json => dispatch(
        {
          type: 'VALIDATE_USER',
          response: json
        }
      ))
    }
}

const getGraph = payload => {
  return dispatch => {
    dispatch(startingRequest());
    request(payload, null).then(json => dispatch(finishedRequest(json)));
  };
};

const getTodosQuery = gql`
{
  todos {
    id
    action
    completed
  }
}`

const validateUserQuery = gql`
query ValidateUser ($name: String, $password: String) {
    validateUser(name: $name, password: $password) {
      id
      name
  }
}`

const createTodoQuery = gql`
mutation Mutation($input: TodoInput!) {
  createTodo(input: $input) {
    id
    action
    completed
  }
}`

const toggleTodoQuery = gql`
mutation Mutation($todoId: ID!) {
  toggleTodo(todoId: $todoId) {
    id
    action
    completed
  }
}`

const updateTodoQuery = gql`
mutation Mutation($input: TodoInput!) {
  updateTodo(input: $input) {
    id
    action
    completed
  }
}`

const deleteTodoQuery = gql`
mutation Mutation($todoId: ID!) {
  deleteTodo(todoId: $todoId) {
    id
    action
    completed
  }
}`

export const getTodos = () => getGraph(getTodosQuery)

const mutate = (payload, variables) => request(payload, variables);

export const addTodo = (action) => {
  return dispatch => {
    mutate(createTodoQuery, {  
      "input": {
        "todoId": null,
        "action": action,
        "completed":false
      }
  }).then(res => {
      const { id, action } = res.createTodo;
      dispatch({
        type: 'ADD',
        id,
        action,
      });
    });
  };
};

export const updateTodo = (id, action) =>
  mutate(updateTodoQuery, {  
    "input": {
      "todoId": id,
      "action": action
    }
});

export const updateCompleted = id =>
mutate(toggleTodoQuery, {
  todoId: id,
});

export const deleteTodo = id => 
mutate(deleteTodoQuery, {
  todoId: id,
});
